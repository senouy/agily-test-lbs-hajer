import express from "express";
import dotenv from "dotenv";
import bodyParser from "body-parser";
import request from "sync-request";
import NodeCache from "node-cache";
dotenv.config();
const app = express();
const port = `${process.env.PORT}`;
const apiKey = `${process.env.API_KEY}`;
const cacheKeyGeocode = `${process.env.CACHE_KEY_GEOCODE}`;
const cacheKeyDailyForcast = `${process.env.CACHE_KEY_DAILY_FORCAST}`;
const myCache = new NodeCache();

app.use(bodyParser.json());

app.post("/geocode", async function (req, res) {
  
  if (myCache.has(cacheKeyGeocode)) {
    console.log("Retrieved value from cache !!");
    res.send(myCache.get(cacheKeyGeocode));
  } else {
    const city = req.body.city;

    const url = `http://api.openweathermap.org/geo/1.0/direct?q=${city}&appid=${apiKey}`;

    const result = request("GET", url);
    const data = JSON.parse(result.getBody("utf8"));

    myCache.set(cacheKeyGeocode, {
      longitude: data[0].lat,
      latitude: data[0].lon,
    });
    res.send({
      longitude: data[0].lat,
      latitude: data[0].lon,
    });
  }
});

app.post("/dailyForecast", function (req, res) {
  const lat = req.body.lat;
  const lon = req.body.lon;
  if (myCache.has(cacheKeyDailyForcast)) {
    console.log("Retrieved value from cache !!");
    res.send(myCache.get(cacheKeyDailyForcast));
  } else {
    let url = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=minutely,hourly,alerts&appid=${apiKey}`;

    const result = request("GET", url);
    const data = JSON.parse(result.getBody("utf8"));
    myCache.set(cacheKeyDailyForcast, data);
    res.json(data);
  }
});
app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
