import React from "react";
import { getcityData } from "../api/weather-api";
import "../App.css";

const SearchBox = () => {
  const [query, setQuery] = React.useState("");
  const [results, setResults] = React.useState([]);
  const onChange = (e) => {
    const { value } = e.target;
    setQuery(value);
    if (value.length > 3) {
      getcityData(value).then((res) => {
        setResults(res);
      });
    }
  };
  return (
    <div>
      <div class="searchInput">
        <input
          type="text"
          placeholder="Search"
          className="textSearch"
          value={query}
          onChange={onChange}
        ></input>
        <img src="../icons/iconSearch.svg" alt="search" />
      </div>
      {query.length > 3 && (
        <ul className="listCountry">
          {results.length > 0 ? (
            results.map((city, index) => (
              <li key={index}>
                <a
                  className="countryItem"
                  href={`/${city.name}/${city.lat}/${city.lon}`}
                  onClick={() => setQuery(city.name)}
                >
                  {city.name}
                  {city.state ? `, ${city.state}` : ""}{" "}
                  <span>({city.country})</span>
                </a>
              </li>
            ))
          ) : (
            <li>no data</li>
          )}
        </ul>
      )}
    </div>
  );
};

export default SearchBox;
