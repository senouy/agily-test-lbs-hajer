import React, { useEffect, useState } from "react";
import "./styles.css";

import { Link, useParams } from "react-router-dom";
import { getExtendedForecastData } from "../../api/weather-api";
import CurrentWeather from "./weatherDay";
import ForecastItem from "./forecastItem";
import { getNextSevenDays } from "../../utils/functions";

const Forecast = () => {
  const [dailyData, setDailyData] = useState();
  const [currentWeather, setCurrentWeather] = useState();

  const { lat, lon } = useParams();

  useEffect(() => {
    getExtendedForecastData(lat, lon).then((res) => {
      setDailyData(res?.daily);
      setCurrentWeather(res?.current);
    });
  }, [lat, lon]);
  return (
    <div className="page">
      <div className="wrapper">
        <div className="firstRow">
          <div className="return">
            <Link to={"/"}>
              <img src="/icons/arrow.png" alt="" height="30" width="30" />
            </Link>
          </div>
          ​
          <div className="listDays">
            {dailyData &&
              dailyData.map((item, index) => (
                <ForecastItem
                  key={index}
                  temp={item.temp.day}
                  icon={item.weather[0].icon}
                  date={item.dt}
                  day={getNextSevenDays()[index].day}
                  weekday={getNextSevenDays()[index].weekday}
                  month={getNextSevenDays()[index].month}
                />
              ))}
          </div>
        </div>
        {currentWeather && (
          <CurrentWeather
            tempDay={currentWeather.temp}
            tempNight={currentWeather.temp}
            humidity={currentWeather.humidity}
            pressure={currentWeather.pressure}
            wind_speed={currentWeather.wind_speed}
          />
        )}
      </div>
    </div>
  );
};

export default Forecast;
