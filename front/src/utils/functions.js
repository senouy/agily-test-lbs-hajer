export function formatDate(time) {
return new Date(time).toLocaleDateString('fr-CA', { weekday:"long", month:"long", day:"numeric"})
}

export function getNextSevenDays() {
  const days = [
    "Dimanche",
    "Lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi",
  ];

  const nextDays = [];
  for (let i = 0; i <8; i++) {
    const weekday = new Date(
      Date.now() + (i + 1) * 24 * 60 * 60 * 1000
    ).getDate();

    const month = new Date(
      Date.now() + (i + 1) * 24 * 60 * 60 * 1000
    ).toLocaleString("fr-CA", { month: "long" });
    
    nextDays.push({
      day: days[new Date(Date.now() + (i + 1) * 24 * 60 * 60 * 1000).getDay()],
      weekday: weekday,
      month: month,
    });
  }
  return nextDays;
}
