import React from "react";
import "../App.css";
import SearchBox from "../components/SearchBox";

const Search= () => {

  return (
    <div className="App">
      <div className="container">
        <h1
          className="titleForcast"
        >
          The Forecast <br /> Weather App
        </h1>
        <SearchBox />
      </div>
    </div>
  );
}

export default Search;