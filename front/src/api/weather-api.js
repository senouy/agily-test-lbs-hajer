import axios from "axios";

export const getcityData = async (city) => {
  let url = `http://api.openweathermap.org/geo/1.0/direct?q=${city}&appid=${process.env.API_KEY}`;
  return axios.get(url).then((response) => {
    return response.data;
  });
};

export const getExtendedForecastData = async (lat, lon) => {
  let url = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=minutely,hourly,alerts&appid=${process.env.API_KEY}`;

  return axios.get(url).then((response) => {
    return response.data;
  });
};
